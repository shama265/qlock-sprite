# QLOCK Sprite #

QLOCK Sprite is a very simple time-stamping web application.
All timestamps are saved to your device, without connecting internet.
You can try it on [my website](https://qlock.rzna.work/).

### Features ###

* Record timestamps on your device

### Usage ###

* Click "STAMP" button to record time when clicking.
* Records are saved in your [local storage](https://developer.mozilla.org/docs/Web/API/Web_Storage_API).
* If you want, you can also delete timestamp records.

### Config
* On-Load Stamp
    * Stamping automatically when accessing this app.
    * Default: off
* Auto Read-more
    * Loading more records automatically when scrolled down.
    * Default: off

------

# QLOCK Sprite #

QLOCK Sprite は、非常にシンプルなタイムスタンプWebアプリケーションです。
すべてのタイムスタンプはあなたの端末上に保存され、インターネットと通信することはありません。
[作者のWebサイト](https://qlock.rzna.work/)で動作を確認できます。

### 機能

* あなたの端末にタイムスタンプを記録

### 使い方

* "STAMP"ボタンをクリックすると、クリックした時刻が記録されます。
* 記録はあなたの[ローカルストレージ](https://developer.mozilla.org/docs/Web/API/Web_Storage_API)に保存されます。
* 望むならば、タイムスタンプの記録を削除することもできます。

### 設定項目

* On-Load Stamp
    * アプリケーションにアクセスした時に自動でスタンプします。
    * 初期値: 無効
* Auto Read-more
    * 下までスクロールすると自動的に追加の記録を読み込みます。
    * 初期値: 無効
