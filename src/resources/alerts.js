export default {
  d01: {
      id: 'd01',
      type: 'danger',
      message: 'Oops, \
      <a class="alert-link" href="https://developer.mozilla.org/ja/docs/Web/API/Web_Storage_API" target="_blank">Local Storage</a> is unavailable, \
      this app will not work well.'
  },
  p01: {
    id: 'p01',
    type: 'primary',
    message: '<strong>Note:</strong>\
    If you are using a web browser with private(incognito) mode, \
    your records will not be saved.\
    <a class="alert-link" href="https://developer.mozilla.org/docs/Web/API/Web_Storage_API#Private_Browsing_Incognito_modes" target="_blank">more info</a>'
  },
}
