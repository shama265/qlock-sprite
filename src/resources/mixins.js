import common from './common.js'
export { recordComponentBase }

var recordComponentBase = {
  data: () => ({
    stamps: [],
    stampFilters: {},
    viewLimit: common.VIEW_LIMIT_DEFAULT,
    alerts: [],
    datePattern: /(\d{4})-(\d{2})-(\d{2})/,
    preferences: {
      onLoadStamp: false,
      autoReadMore: false,
    },
  }),
  computed: {
    stampsView: function() {
      let view = this.stamps;
      let from = this.dateFromNorm;
      let to = this.dateToNorm;
      if(from) {
        view = view.filter(d => d >= new Date(from.year, from.monthIdx, from.date));
      }
      if(to) {
        view = view.filter(d => d <= (new Date(to.year, to.monthIdx, to.date + 1)));
      }
      return view;
    },
    stampsSliced: function() {
      return this.stampsView.slice(0, this.viewLimit);
    },
    dateFromNorm: function() {
      return this.parseDate(this.stampFilters.dateFrom);
    },
    dateToNorm: function() {
      return this.parseDate(this.stampFilters.dateTo);
    },
    isFilterEnabled: function() {
      return Object.keys(this.stampFilters).length > 0;
    },
    filterBtnClass: function() {
      return { 
        'btn-info': this.isFilterEnabled,
        'btn-secondary': !this.isFilterEnabled,
      };
    },
  },
  filters: {
    timestamp: function (value) {
      var date = new Date(value);
      return date.toLocaleString();
    }
  },
  methods: {
    saveStamps: function(newStamps) {
      window.localStorage.setItem('stamps', JSON.stringify(newStamps));
    },
    readMore: function() {
      this.viewLimit += common.VIEW_LIMIT_PLUS;
    },
    resetLimit: function() {
      this.viewLimit = common.VIEW_LIMIT_DEFAULT;
    },
    resetFilter: function() {
      this.stampFilters = {};
    },
    parseDate: function(dateString) {
      let result = this.datePattern.exec(dateString);
      if(result) {
        result = {
          year: parseInt(result[1]),
          monthIdx: parseInt(result[2]) - 1,
          date: parseInt(result[3]),
        };
      }
      return result;
    },
    infiniteHandler: function($state) {
      if (this.viewLimit < this.stampsView.length) {
        this.readMore();
        $state.loaded();
      } else {
        $state.complete();
      }
    },
  },
}
