import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '../views/Main.vue'
import Config from '../views/Config.vue'
import Delete from '../views/Delete.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main
  },
  {
    path: '/config',
    name: 'Config',
    component: Config
  },
  {
    path: '/delete',
    name: 'Delete',
    component: Delete
  },
]

const router = new VueRouter({
  routes
})

export default router
