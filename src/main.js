import Vue from 'vue'
import InfiniteLoading from 'vue-infinite-loading';

import router from './router'
import App from './App.vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faStopwatch, faFilter, faEraser, faTrash, faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { faCircle as farCircle } from '@fortawesome/free-regular-svg-icons' 
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faStopwatch, faFilter, faEraser, faTrash, faCheckCircle, farCircle)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

Vue.use(InfiniteLoading, {})

new Vue({
  router,

  render: e => e(App)
}).$mount('#app')
